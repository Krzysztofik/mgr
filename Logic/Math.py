import cv2
import numpy
from sklearn.neighbors import KNeighborsClassifier


def avg_pixel_color(skin):
    r_list = []
    g_list = []
    b_list = []
    for row in skin:
        for rgb in row:
            l_rgb = []
            for x in rgb:
                l_rgb.append(x)
            if l_rgb != [0, 0, 0]:
                r_list.append(l_rgb[0])
                g_list.append(l_rgb[1])
                b_list.append(l_rgb[2])
    avg_r = int(sum(r_list) / len(r_list))
    avg_g = int(sum(g_list) / len(g_list))
    avg_b = int(sum(b_list) / len(b_list))
    return avg_r, avg_g, avg_b


def knn(image, skin, x, xw, y, yh):
    list_feature = []
    list_labels = []
    for column in range(skin.shape[0]):
        for row in range(skin.shape[1]):
            if (y + yh < column or column < y) and (x + xw < row or row < x):
                pixel = skin[column, row]
                im = image[column, row]
                if pixel[0] == 0 and pixel[1] == 0 and pixel[2] == 0:
                    list_feature.append([im[0], im[1], im[2]])
                    list_labels.append(0)
                else:
                    list_feature.append([im[0], im[1], im[2]])
                    list_labels.append(1)
    beard_skin = skin[y:y + yh, x:x + xw]
    beard_frame = image[y:y + yh, x:x + xw]
    neigh = KNeighborsClassifier(n_neighbors=7)
    neigh.fit(list_feature, list_labels)
    avg_beard = 0
    color_pixel = 1
    for raw in range(beard_skin.shape[0]):
        for rgb in range(beard_skin.shape[1]):
            r = beard_skin[raw, rgb, 0]
            g = beard_skin[raw, rgb, 1]
            b = beard_skin[raw, rgb, 2]
            if r == 0 or g == 0 or b == 0:
                color_pixel = color_pixel + 1
                count = neigh.predict([[beard_frame[raw, rgb, 0], beard_frame[raw, rgb, 1], beard_frame[raw, rgb, 2]]])
                if count == 1:
                    avg_beard = avg_beard + 1

    print("beard : ", avg_beard / color_pixel * 100)
    return avg_beard / color_pixel * 100


def clean_black_board(image):
    cv2.imshow("aa", image)
    sum = image.sum(axis=0)
    counter = 0
    for x in sum:
        if x[0] == 0 and x[1] == 0 and x[2] == 0:
            counter = counter + 1
        else:
            break
    revcounter = 0
    for x in reversed(sum):
        if x[0] == 0 and x[1] == 0 and x[2] == 0:

            revcounter = revcounter + 1
        else:
            break
    if image.shape[1] == counter:
        return image, 0, 0
    else:
        return image[:, counter:image.shape[1] - revcounter, :], counter, revcounter
