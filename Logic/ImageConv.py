import cv2
import numpy as np
import angus.client
from pprint import pprint
from Logic.Math import avg_pixel_color, knn, clean_black_board

nose_data = "C:\\Users\\szymo\\Desktop\\Test magisterka\\src\\haarcascade_mcs_nose.xml"
eyeData = "C:\\Users\\szymo\\Desktop\\Test magisterka\\src\\eyes.xml"
faceData = "C:\\Users\\szymo\\Desktop\\Test magisterka\\src\\haarcascade_frontalface_default.xml"
googles_data = "C:\\Users\\szymo\\Desktop\\Test magisterka\\src\\glasses.xml"
mouth_data = "C:\\Users\\szymo\\Desktop\\Test magisterka\\src\\haarcascade_mcs_mouth.xml"
eyeglass="C:\\Users\\szymo\\Desktop\\Test magisterka\\src\\haarcascade_eye_tree_eyeglasses.xml"

# left_ears = "C:\\Users\\szymo\\Desktop\\Test magisterka\\src\\haarcascade_mcs_leftear.xml"
# right_ears = "C:\\Users\\szymo\\Desktop\\Test magisterka\\src\\haarcascade_mcs_rightear.xml"


def checkImage(image_bytes):
    # check that imaga has person face
    face_cascade = cv2.CascadeClassifier('src\\haarcascade_frontalface_default.xml')

    nparr = np.fromstring(image_bytes, np.uint8)
    image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    return faces


def take_color_skin(image_bytes):
    lower = np.array([0, 48, 80], dtype="uint8")
    upper = np.array([20, 255, 255], dtype="uint8")
    np_arr = np.fromstring(image_bytes, np.uint8)
    image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

    converted = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    skin_mask = cv2.inRange(converted, lower, upper)

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
    skin_mask = cv2.erode(skin_mask, kernel, iterations=1)
    skin_mask = cv2.dilate(skin_mask, kernel, iterations=1)

    # mask to the frame
    skin_mask = cv2.GaussianBlur(skin_mask, (3, 3), 0)
    skin = cv2.bitwise_and(image, image, mask=skin_mask)

    # show the skin in the image along with the mask
    cv2.imshow("images", np.hstack([image, skin]))
    return skin


def detectionglassEye(image_bytes):
    nparr = np.fromstring(image_bytes, np.uint8)
    frame = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    canny2 = cv2.Canny(gray, 100, 300, 3)
    # cv2.imshow("ad", canny2)

    skin = take_color_skin(image_bytes)
    detect_moustache(frame, skin)
    detect_beard(frame, skin)
    detect_gender(frame)
    # detect_ear(frame)
    roi_eye_gray = detect_eyes(gray)
    low_threshold = 100
    ratio = 3
    kernel_size = 3
    kernel = np.ones((5, 5), np.uint8)
    canny = cv2.Canny(roi_eye_gray, low_threshold, low_threshold * ratio, kernel_size)
    # cv2.imshow("ad", canny)
    mask = cv2.dilate(canny, np.ones((5, 5), np.uint8), iterations=3)
    mask = cv2.erode(mask, np.ones((5, 5), np.uint8), iterations=3)

    # cv2.imshow("mask", mask)
    outerFrame = cv2.normalize(src=mask, dst=None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
    glassMask = cv2.erode(outerFrame, np.ones((5, 5), np.uint8))
    cleanedOuter = cv2.dilate(glassMask, np.ones((5, 5), np.uint8))
    glassInner = cv2.dilate(glassMask, np.ones((5, 5), np.uint8))
    inneGlass = cv2.normalize(src=mask, dst=None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
    inneGlass = cv2.erode(inneGlass, np.ones((5, 5), np.uint8))
    fullGlassesMask = cleanedOuter - inneGlass

    # cv2.imshow("massglass",fullGlassesMask)
    skin = cv2.bitwise_and(roi_eye_gray, roi_eye_gray, mask=fullGlassesMask)
    # cv2.imshow("roi adfaasdfa",fullGlassesMask)
    eye_class = cv2.CascadeClassifier(eyeData)
    gg=roi_eye_gray.copy()
    eyes = eye_class.detectMultiScale(gg)
    for y in range(fullGlassesMask.shape[0]):
        for x in range(fullGlassesMask.shape[1]):
            if fullGlassesMask[y,x]==255:
                roi_eye_gray[y,x]=0

    rectangle=0
    size_eye = []
    for (ex, ey, ew, eh) in eyes:
        cv2.rectangle(gg, (ex, ey), (ex + ew, ey + eh), 255, 1)
        size_eye.append([ey, ey + eh, ex, ex + ew])
        rectangle=2*ew+2*eh

    if rectangle!=0:
        counter=0
        for y in range(gg.shape[0]):
            for x in range(gg.shape[1]):
                if gg[y,x]==255 and roi_eye_gray[y,x]==0:
                  counter=counter+1
        print( counter, rectangle, counter/rectangle*100)
        if 21>counter/rectangle*100>14:
            print("glass22")
    else:
        cv2.imshow("", roi_eye_gray)
        mask_max = 0
        mask_min = fullGlassesMask.shape[1]

        for row in fullGlassesMask:
            for x in range(row.size):
                if row[x] != 0:
                    if x > mask_max:
                        mask_max = x
                    if x < mask_min:
                        mask_min = x
        face = fullGlassesMask[:, mask_min:mask_max]
        max_counter = 0
        list_long = []
        for row in face:
            counter = 0
            for x in row:
                if x != 0:
                    counter = counter + 1
            if counter > max_counter:
                max_counter = counter
            list_long.append(counter)
        list_long.sort(reverse=True)
        log = ((list_long[0] + list_long[1]+ list_long[2] + list_long[3] )/2 )
        need_size = face.shape[1]
        # print(need_size / 2, max_counter)
        procent = log / need_size * 100
        face_cascade = cv2.CascadeClassifier(eyeglass)
        d=face_cascade.detectMultiScale(face)
        print("procent : ",procent)
        if 68<= procent <= 78 or 87 <= procent<93 or 40.5<procent <45:
            print("glass")

    # cv2.imshow("adfa", frame)


def detect_mouth(frame):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    mouth_cascade = cv2.CascadeClassifier(mouth_data)
    mouth_rect = mouth_cascade.detectMultiScale(gray, 1.3, 5)
    for (x, y, w, h) in mouth_rect:
        y = int(y - 0.15 * h)
        return y, x, h, w
    # return frame
    return None, None, None, None


def detect_nose(frame):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # cv2.imshow("a", gray)
    nose_cascade = cv2.CascadeClassifier(nose_data)
    noose_rect = nose_cascade.detectMultiScale(gray)
    for (x, y, w, h) in noose_rect:
        y = int(y - 0.15 * h)
        return y, x, h, w
    # return
    return None, None, None, None


def detect_eyes(roi_gray):
    eye_class = cv2.CascadeClassifier(eyeData)
    eyes = eye_class.detectMultiScale(roi_gray)

    size_eye = []
    for (ex, ey, ew, eh) in eyes:
        # cv2.rectangle(roi_gray, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

        size_eye.append([ey, ey + eh, ex, ex + ew])
    # cv2.imshow("adfaaaaaaaaaaa", roi_gray)
    if size_eye.__len__() != 0:
        size_eye = np.array(size_eye)
        size_eye_min = size_eye.min(0)
        size_eye_max = size_eye.max(0)
        avg_width = int((size_eye_max[1] - size_eye_min[0]) / 2)
        avg_height = int((size_eye_max[3] - size_eye_min[2]) / 2)
        if roi_gray.shape[0] > size_eye_max[3] + avg_height and roi_gray.shape[1] > size_eye_max[1] + avg_width:
            if size_eye.shape[0] > 1:
                roi_eye_gray = roi_gray[size_eye_min[0] - avg_height:size_eye_max[1] + avg_height,
                               0:]
                roi_eye_color = roi_gray[size_eye_min[0] - avg_height:size_eye_max[1] + avg_height,
                                0:]
            else:
                roi_eye_gray = roi_gray[size_eye_min[0] - 2 * avg_height:size_eye_max[1] + 2 * avg_height,
                               0:]
                roi_eye_color = roi_gray[size_eye_min[0] - 2 * avg_height:size_eye_max[1] + 2 * avg_height,
                                0:]
        else:
            roi_eye_gray = roi_gray[size_eye_min[0]:size_eye_max[1] + avg_height,
                           0:]
            roi_eye_color = roi_gray[size_eye_min[0]:size_eye_max[1] + avg_height,
                            0:]
        return roi_eye_gray
    return roi_gray


def detect_moustache(frame, skin):
    y_nose, x_nose, h_nose, w_nose = detect_nose(frame)
    y_mouth, x_mouth, h_mouth, w_mouth = detect_mouth(frame)

    if y_nose is not None and y_mouth is not None:
        yy_nose = y_nose + h_nose
        xx_nose = x_nose + w_nose
        yy_mouth = y_mouth + h_mouth
        xx_mouth = x_mouth + w_mouth

        if (y_nose < yy_mouth) and (x_nose < xx_mouth):
            skin_small = skin[y_nose:yy_mouth, x_nose:xx_mouth]

        if (y_nose < yy_mouth) and (xx_nose > x_mouth):
            skin_small = skin[y_nose:yy_mouth, x_mouth:xx_nose]

        if (yy_nose > y_mouth) and (x_nose < xx_mouth):
            skin_small = skin[y_mouth:yy_nose, x_nose:xx_mouth]

        if (yy_nose > y_mouth) and (xx_nose > x_mouth):
            skin_small = skin[y_mouth:yy_nose, x_mouth:xx_nose]

        gray = cv2.cvtColor(skin_small, cv2.COLOR_BGR2GRAY)
        # cv2.imshow("asdaff", gray)
        maks_row = 0
        for row in gray:
            cout = 0
            for x in range(row.size):
                if row[x] == 0:
                    cout = cout + 1
            if cout > maks_row:
                maks_row = cout
        if maks_row > gray.shape[1] / 2:
            print("moustache1")

    else:
        if x_nose is not None:

            avg_r, avg_g, avg_b = avg_pixel_color(skin)

            frame_under_nose = skin[y_nose + h_nose:y_nose + h_nose + 10, x_nose - 10:x_nose + w_nose + 10]
            par = 0.15
            avg_beard = 0
            color_pixel = 0
            for raw in frame_under_nose:
                for rgb in raw:
                    r = rgb[0]
                    g = rgb[1]
                    b = rgb[2]
                    if r != 0 or g != 0 or b != 0:
                        color_pixel = color_pixel + 1
                        if (r + par * r > avg_r > r - par * r) and (g + par * g > avg_g > g - par * g) and (
                                b + par * b > avg_b > b - par * b):
                            avg_beard = avg_beard + 1
            if (color_pixel - avg_beard) / 2 > avg_beard:
                print("moustache2")
        else:
            print("can't take parameter to mustasche")


def detect_beard(frame, skin):
    y_nose, x_nose, h_nose, w_nose = detect_nose(frame)
    if y_nose is None:
        print(" Can't detect nose")
        return
    mouth_width = 3 * w_nose
    mouth_start_point_x = int(x_nose - w_nose )
    mouth_start_point_y = y_nose + 2 * h_nose
    mouth_height = 40
    beard_frame = skin[mouth_start_point_y:mouth_start_point_y + mouth_height,
                  mouth_start_point_x:mouth_start_point_x + mouth_width]
    cv2.imshow("beard", beard_frame)
    beard_frame,x,y=clean_black_board(beard_frame)
    if x==0 and y==0:
        print("beard")

    cv2.imshow("beard",beard_frame)
    cv2.imshow("bead skin",skin)
    cout = knn(frame, skin, mouth_start_point_x+x, mouth_width-y, mouth_start_point_y, mouth_height)
    print(cout)
    if 0.0<cout<2:
        print("beard")



def detect_ear(frame):
    y_nose, x_nose, h_nose, w_nose = detect_nose(frame)
    if y_nose is None:
        print(" Can't detect nose")
        return
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    eye_class = cv2.CascadeClassifier(eyeData)
    eyes = eye_class.detectMultiScale(gray)

    size_eye = []
    for (ex, ey, ew, eh) in eyes:
        size_eye.append([ey, ey + eh, ex, ex + ew])
    size_eye = np.array(size_eye)
    size_eye_min = size_eye.min(0)

    earn = frame[size_eye_min[0]:y_nose + h_nose, :]
    cv2.imshow("ears", earn)
    earn_gray = gray[size_eye_min[0]:y_nose + h_nose, :]
    low_threshold = 100
    ratio = 3
    kernel_size = 3
    canny = cv2.Canny(gray, low_threshold, low_threshold * ratio, kernel_size)
    # cv2.imshow("canny full",canny)
    canny = cv2.Canny(earn_gray, low_threshold, low_threshold * ratio, kernel_size)
    # cv2.imshow("canny earn", canny)
    cv2.imwrite("C:\\Users\\szymo\\Desktop\\Test magisterka\\src\\image.jpg", canny)


def detect_gender(image):
    cv2.imwrite("C:\\Users\\szymo\\Desktop\\Test magisterka\\src\\image.jpg", image)
