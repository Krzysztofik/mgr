import re

import phonenumbers


class TextConvert():
    __slots__ = "text", "person"

    __tags = ["JĘZYKI OBCE", "ZAINTERESOWANIA", 'WYKSZTAŁCENIE', "DODATKOWE\nKWALIFIKACJE", "CEL ZAWODOWY",
              "UMIEJĘTNOŚCI \nZAWODOWE", "DOŚWIADCZENIE \nZAWODOWE",
              "DODATKOWE \nINFORMACJE", 'DOŚWIADCZENIE', 'OSIĄGNIĘCIA',
              "UMIEJĘTNOŚCI", "WYRAŻAM ZGODĘ"]
    __tags2 = ["JĘZYKI OBCE", "ZAINTERESOWANIA", 'WYKSZTAŁCENIE', "DODATKOWE\nKWALIFIKACJE", "CEL ZAWODOWY",
               "UMIEJĘTNOŚCI \nZAWODOWE", "DOŚWIADCZENIE \nZAWODOWE",
               "DODATKOWE \nINFORMACJE", 'DOŚWIADCZENIE', 'OSIĄGNIĘCIA',
               "UMIEJĘTNOŚCI\n", "WYRAŻAM ZGODĘ"]

    __regex = r'(\d+ – \d+)|(\d{2}. \d{4})|(\d{2}.\d{4})|(\d+\.\d+ – OBECNIE)|(\d+.\d+ – \d+.\d+)|(\d+ – \d+ .\d+)|(\d{4} - \d{4})|(\d{4})'

    def __init__(self, text, person):
        self.text = str(text)
        self.person = person
        self.__getEmail()
        self.__getNumber()
        self.__getBirthday()
        self.__getNameSurname()
        self.__getInformation()

    def __getEmail(self):
        self.person.email = re.findall(r'[\w\.-]+@[\w\.-]+', self.text)

    def __getNumber(self):
        self.person.number = []
        for match in phonenumbers.PhoneNumberMatcher(self.text, "PL"):
            self.person.number.append(match.raw_string)

    def __getBirthday(self):
        self.person.birthday = re.findall(r'[0-9]{2}\.[0-9]{2}\.[0-9]{4}', self.text)[0]

    def __getNameSurname(self):
        data = ""
        textCV = re.sub(' +', ' ', self.text)
        textCV = textCV.upper()
        cv = re.findall('((CURRICULUM VITAE)( \(?CV\)?)?)', textCV)
        if (cv != []):
            textCV = textCV.split(cv[0][0])

            if (textCV[0] != '' and re.sub(' +', ' ', re.sub('\n+', '', textCV[0])) != ' ' and re.sub('\n+', '',
                                                                                                      re.sub('\n+', '',
                                                                                                             textCV[
                                                                                                                 0])) != ''):
                textCV = re.sub('\n+', '', textCV[0])
                data = re.sub(' +', ' ', textCV)
                data = re.findall(r'\w+', data)
            else:
                textCV = textCV[1].split('\n')
                for text in textCV:
                    if text != '' and text != ' ':
                        data = re.findall(r'\w+', text)
                        break

        self.person.name = data[0]
        self.person.surname = ""
        for sur in data[1::]:
            self.person.surname += sur

    def __getInformation(self):
        text = self.text.upper()
        flag = False
        tags = []
        if re.search(r'(CEL ZAWODOWY)', text) is None:
            text = text.split("\n")
            self.__withoutGoal(text)
            tags = self.__tags
        else:
            text = text.split("\n")
            self.__withGoal(text)
            tags = self.__tags2
        tem = ""
        for tt in text:
            tem += tt
            tem += "\n"
        text = tem
        lista = []
        for tag in tags:
            if type(text) == str:
                text = list(text.split(tag))
                text[1]=str(tag)+" "+text[1]
            else:
                temp = []
                for e in text:
                    temp_e = e.split(tag)
                    if len(temp_e) != 1:
                        temp_e[1] = str(tag) + "\n" + temp_e[1]
                    temp.append(temp_e)
                # temp = list([element.split(tag) for element in text])
                text = []
                for t in temp:
                    for x in t:
                        text.append(x)

        temp = ''
        returnText = []
        for linetext in text[:-1]:
            if temp != '':
                linetext = temp + linetext
                temp = ''
            line = linetext.split('\n')
            while True:
                if len(line) > 0 and line[-1] == '':
                    line.pop(-1)
                else:
                    break
            returnText.append(linetext)
        self.person.intresting = []
        for text in returnText:
            if re.search(r'(DOŚWIADCZENIE \nZAWODOWE)|(DOŚWIADCZENIE)',text):
                self.person.jobs=text
            if re.search(r'(DODATKOWE\nKWALIFIKACJE)|(UMIEJĘTNOŚCI)',text):
                self.person.intresting.append(text)

    def __withoutGoal(self, text):
        for x in range(len(text)):
            if text[x] == "JĘZYKI OBCE" and text[x + 2] == "ZAINTERESOWANIA":
                match = None
                i = 1
                while match is None:
                    match = re.search('UMIEJĘTNOŚC', text[x - i])
                    if match is None:
                        i = i + 1
                for y in range(i - 1):
                    t = text[x - y - 1]
                    text[x - y - 1] = text[x - y]
                    text[x - y] = t

        for x in range(len(text)):
            if text[x] == "DOŚWIADCZENIE":
                i = 1
                match = None
                while match is None and i < 4:
                    match = re.search(self.__regex, text[x - i])
                    if match is None:
                        i = i + 1
                if i < 4:
                    for y in range(i):
                        t = text[x - y - 1]
                        text[x - y - 1] = text[x - y]
                        text[x - y] = t
            if text[x] == "UMIEJĘTNOŚCI":
                match = re.search(self.__regex, text[x - 1])
                if match is None:
                    i = 1
                    while match is None:
                        match = re.search(self.__regex, text[x - i])
                        if match is None:
                            i = i + 1
                    for y in range(i - 1):
                        t = text[x - y - 1]
                        text[x - y - 1] = text[x - y]
                        text[x - y] = t
        for x in range(len(text)):
            if text[x] == "ZAINTERESOWANIA":
                t = text[x - 1]
                text[x - 1] = text[x]
                text[x] = t

            if text[x] == "WYKSZTAŁCENIE" or (text[x] == "DODATKOWE" and text[x + 1] == "KWALIFIKACJE"):
                match = None

                i = 1
                while match is None:
                    match = re.search(self.__regex, text[x - i])
                    if match is None:
                        i = i + 1
                if text[x] == "DODATKOWE" and text[x + 1] == "KWALIFIKACJE":
                    text.pop(x + 1)
                    text.append("")
                    text[x] = "DODATKOWE\nKWALIFIKACJE"
                for y in range(i):
                    t = text[x - y - 1]
                    text[x - y - 1] = text[x - y]
                    text[x - y] = t
        for x in range(len(text)):
            if text[x] == "DODATKOWE\nKWALIFIKACJE":
                j = 0
                while not self.__tags.__contains__(text[x + j + 1]):
                    j = j + 1
                count = 0
                for y in range(j):
                    match = re.search(self.__regex, text[x + y + 1])
                    if match is None:
                        count = count - 1
                    else:
                        count = count + 1
                for y in range(abs(count)):
                    t = text[x - y - 1]
                    text[x - y - 1] = text[x - y]
                    text[x - y] = t

        for x in range(len(text)):
            if text[x] == "OSIĄGNIĘCIA":
                i=1
                match = None
                if self.__tags.__contains__(text[x + 1]):
                    count=0
                    while match is None and count!=2:
                        match = re.search(self.__regex, text[x - i])
                        if match is None:
                            i = i + 1
                        else:
                            count=count+1
                            i=i+1
                            match=None
                    i=i-1
                else:
                    while match is None:
                        match = re.search(self.__regex, text[x - i])
                        if match is None:
                            i = i + 1
                for y in range(i):
                    t = text[x - y - 1]
                    text[x - y - 1] = text[x - y]
                    text[x - y] = t

        for x in range(len(text)):
            if text[x] == "WYKSZTAŁCENIE":
                for z in reversed(range(x)):
                    match = re.search(r'(POLITECHNIKA)|(UNIWERSYTET)|(AKADEMIA)|(UCZELNIA)', text[z])
                    if match is not None:
                        count = x - z
                        for y in range(count):
                            t = text[x - y - 1]
                            text[x - y - 1] = text[x - y]
                            text[x - y] = t
        return text

    def __withGoal(self, text):
        for x in range(len(text)):
            if text[x] == "CEL ZAWODOWY":
                text[x - 1] = text[x] + " " + text[x - 1]
                text[x] = ""
            if text[x] == "UMIEJĘTNOŚCI " or text[x] == "UMIEJĘTNOŚCI" and text[x + 1] == "ZAWODOWE":
                text.pop(x + 1)
                text.append("")
                text[x] = "UMIEJĘTNOŚCI \nZAWODOWE"
                match = None
                i = 1
                while match is None:
                    match = re.search(r'(CEL ZAWODOWY)', text[x - i])
                    if match is None:
                        i = i + 1
                for y in range(i - 1):
                    t = text[x - y - 1]
                    text[x - y - 1] = text[x - y]
                    text[x - y] = t

            if text[x] == "DOŚWIADCZENIE":
                match = None
                i = 1
                while match is None:
                    match = re.search(self.__regex, text[x - i])
                    if match is None:
                        i = i + 1
                for y in range(i + 2):
                    t = text[x - y - 1]
                    text[x - y - 1] = text[x - y]
                    text[x - y] = t
            if text[x] == "WYKSZTAŁCENIE":
                match = None
                counter = 0
                i = 1
                while match is None or counter != 2:
                    match = re.search(self.__regex, text[x - i])
                    i = i + 1
                    if match is not None:
                        counter = counter + 1
                for y in range(i - 2):
                    t = text[x - y - 1]
                    text[x - y - 1] = text[x - y]
                    text[x - y] = t

            if (text[x] == "DODATKOWE" and text[x + 1] == "KWALIFIKACJE") or (
                    text[x] == "DODATKOWE " and text[x + 1] == 'INFORMACJE'):
                text.pop(x + 1)
                text.append("")
                text[x] = "DODATKOWE\nKWALIFIKACJE"
                match = None
                i = 1
                counter = 0
                while match is None and counter != 2:
                    match = re.search(self.__regex, text[x - i])
                    if match is None:
                        i = i + 1
                    else:
                        counter = counter + 1
                        i=i+1
                        match = None
                for y in range(i - 2):
                    t = text[x - y - 1]
                    text[x - y - 1] = text[x - y]
                    text[x - y] = t
            if text[x] == 'DOŚWIADCZENIE ZAWODOWE':
                i = 1
                while match is None and i != 4:
                    match = re.search(self.__regex, text[x - i])
                    if match is None:
                        i = i + 1
                for y in range(i + 2):
                    t = text[x - y - 1]
                    text[x - y - 1] = text[x - y]
                    text[x - y] = t

            if text[x] == "JĘZYKI OBCE":
                match = None
                i = 1
                while match is None:
                    match = re.search(self.__regex, text[x - i])
                    if match is None:
                        i = i + 1
                for y in range(i - 1):
                    t = text[x - y - 1]
                    text[x - y - 1] = text[x - y]
                    text[x - y] = t
                z = i
                match = re.search(self.__regex, text[x - i])
                while match is not None:
                    i = z
                    while match is not None:
                        match = re.search(self.__regex, text[x - i])
                        i = i + 1
                    for y in range(i - 2):
                        t = text[x - y - 3]
                        text[x - y - 3] = text[x - y - 2]
                        text[x - y - 2] = t
                    match = re.search(self.__regex, text[x - z - 1])
        for x in range(len(text)):
            if text[x] == "ZAINTERESOWANIA":
                t = text[x - 1]
                text[x - 1] = text[x]
                text[x] = t

        return text
