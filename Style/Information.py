# from PyQt5.QtGui import  QForm
import re

from PyQt5.QtWidgets import QFormLayout, QLineEdit, QLabel

from Style.Font import FontBold

regex = r'(\d+ – \d+)|(\d{2}. \d{4})|(\d{2}.\d{4})|(\d+\.\d+ – OBECNIE)|(\d+.\d+ – \d+.\d+)|(\d+ – \d+ .\d+)|(\d{4} - \d{4})|(\d{4})'


def window(person):
    flow = QFormLayout()

    lineName = QLineEdit(person.name)
    lineName.setReadOnly(True)
    flow.addRow("Name : ", lineName)

    lineSurname = QLineEdit(person.surname)
    lineName.setReadOnly(True)
    flow.addRow("Surname : ", lineSurname)

    lineBirthDay = QLineEdit(person.birthday)
    lineBirthDay.setReadOnly(True)
    flow.addRow("Birthday :  ", lineBirthDay)

    if len(person.email) == 0:
        lineEmail = QLineEdit("")
    else:
        lineEmail = QLineEdit(person.email[0])
    lineEmail.setReadOnly(True)
    flow.addRow("Email : ", lineEmail)

    if len(person.number) == 0:
        lineNumber = QLineEdit("")
    else:
        lineNumber = QLineEdit(person.number[0])
    lineNumber.setReadOnly(True)
    flow.addRow("Number : ", lineNumber)

    count=0
    for line in person.jobs.split('\n'):
        mach = re.findall(regex,line)
        if mach :
            linejob = QLineEdit(line)
            linejob.setReadOnly(True)
            if count == 0:
                count=count+1
                flow.addRow("Jobs : ", linejob)
            else:
                flow.addRow("", linejob)

    if count==0:
        linejob = QLineEdit("")
        linejob.setReadOnly(True)
        flow.addRow("Job : ", line)

    for skil in person.intresting:
        name=skil.split('\n\n')[0]
        skils=skil.split('\n\n')[1]
        skils=skils.split('\n')
        count=0
        for x in skils:
            mach=re.findall(regex,x)
            if not mach and x!='':
                if count==0:
                    lineSkil = QLineEdit(x)
                    lineSkil.setReadOnly(True)
                    flow.addRow(name+" : ", lineSkil)
                    count=count+1
                else:
                    lineSkil = QLineEdit(x)
                    lineSkil.setReadOnly(True)
                    flow.addRow("", lineSkil)

    return flow


def BadImage(self):
    label=QLabel(self)
    label.setText("Złe zdjecie w CV lub brak zdjęcia ")
    label.setFont(FontBold())
    return label