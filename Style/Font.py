from PyQt5.QtGui import QFont


def FontBold():
    font = QFont()
    font.setPixelSize(20)
    font.setBold(True)
    return font