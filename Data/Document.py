import io
import re

# from pdfminer.converter import TextConverter
# from pdfminer.layout import LAParams
# from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
# from pdfminer.pdfpage import PDFPage


def readDocx(text):
    splitText = text.split('\n')
    convertText = []
    convertText.append('')
    for line in range(len(splitText)):
        if (convertText.__contains__(splitText[line]) == False):
            convertText.append(splitText[line])
        else:
            match=re.search(r'(\d+ – \d+)|(\d{2}. \d{4}|(\d+\.\d+ – obecnie)|(\d+.\d+ – \d+.\d+)|(\d+ – \d+ .\d+))',splitText[line])
            if match!=[] and match!=None and splitText[line]!=splitText[line-2]and splitText[line]!=splitText[line-4]:
                convertText.append(splitText[line])
    returnText = ""
    for singleText in convertText:
        returnText += singleText
        returnText += "\n"
    return returnText


# def convert_pdf_to_txt(path):
#     rsrcmgr = PDFResourceManager()
#     retstr = io.StringIO()
#     codec = 'utf-8'
#     laparams = LAParams()
#     device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
#     fp = open(path, 'rb')
#     interpreter = PDFPageInterpreter(rsrcmgr, device)
#     password = ""
#     maxpages = 0
#     caching = True
#     pagenos = set()
#
#     for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages,
#                                   password=password,
#                                   caching=caching,
#                                   check_extractable=True):
#         interpreter.process_page(page)
#
#     text = retstr.getvalue()
#
#     fp.close()
#     device.close()
#     retstr.close()
#     return text
#
#
# def convert(fname, pages=None):
#     if not pages:
#         pagenums = set()
#     else:
#         pagenums = set(pages)
#
#     output = StringIO()
#     manager = PDFResourceManager()
#     converter = TextConverter(manager, output, laparams=LAParams())
#     interpreter = PDFPageInterpreter(manager, converter)
#
#     infile = open(fname, 'rb')
#     for page in PDFPage.get_pages(infile, pagenums):
#         interpreter.process_page(page)
#     infile.close()
#     converter.close()
#     text = output.getvalue()
#     output.close
#     return text
