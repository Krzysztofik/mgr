import io
import re
import zipfile
from PIL import Image as IM


class Image():
    __slots__ = ['page']

    def __init__(self, page):
        self.page = page

    def getImagePDF(self):
        objects = self.page['/Resources']['/XObject'].getObject()
        for obj in objects:
            if objects[obj]['/Subtype'] == '/Image':
                data = objects[obj]._data
                return data


def get_docx_text(path):
    """
    Take the path of a docx file as argument, return the text in unicode.
    """
    document = zipfile.ZipFile(path)
    for doc in document.NameToInfo:
        if re.findall("image1", doc) and (re.findall(".emf",doc) or  re.findall(".j",doc)):
            xml_content = document.read(doc)
            document.close()
            image = IM.open(io.BytesIO(xml_content))
            image = image.resize((250, 300), IM.ANTIALIAS)
            # image.show()
            output = io.BytesIO()
            image.save(output, format='JPEG')
            return output.getvalue()
