import sys


# import PyPDF2
import docx2txt
# from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog,QLabel
from docx import Document
from Data.Document import readDocx#, convert_pdf_to_txt, convert
from Data.Image import Image, get_docx_text
from Data.Person import Person
from Logic.ImageConv import checkImage, take_color_skin, detectionglassEye
from Logic.TextConvert import TextConvert
from Style.Font import FontBold
from Style.Information import window, BadImage


class App(QMainWindow):
    textMainLabel = "Wczytaj CV"

    def __init__(self):
        super().__init__()
        self.title = 'Image and text analyser'
        self.left = 50
        self.top = 50
        self.width = 640
        self.height = 480
        self.name_image = ""
        self.pictureLabel = QLabel(self)
        self.pictureLabel.move(10, 10)
        self.textLabel = QLabel(self)
        self.infoLabel = QLabel(self)
        self.infoLabelImage=QLabel(self)
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.statusBar().showMessage('Message in statusbar.')
        self.initMenu()
        self.initTextLabel()
        self.show()

    def openFileNameDialog(self):
        path = QFileDialog.getOpenFileName(self, 'Open File', 'C:\\Users\\szymo\\Desktop\\cv mag\\cv1', 'Doc/Pdf(*.docx *.pdf)')
        name = path[0]
        person = Person()
        if (name[-3::1].lower() == 'pdf'):
            file = open(name, 'rb')
            # pdf = PyPDF2.PdfFileReader(file)
            # convert(name)
            # image = Image(pdf.getPage(0))
            # self.name_image = image.getImagePDF()
            # text = convert_pdf_to_txt(name)
            # TextConvert(text, person)
            # print(person.get_str())

        if (name[-4::1].lower() == 'docx'):

            document = Document(name)
            image = get_docx_text(name)
            self.name_image = image
            faces=checkImage(self.name_image)
            if len(faces)==0:
                self.infoLabelImage=BadImage(self)
                detectionglassEye(self.name_image)
            else:
                detectionglassEye(self.name_image)
            text = docx2txt.process(name)
            text = readDocx(text)
            TextConvert(text, person)
            if self.infoLabel.layout() is not None:
                self.infoLabel.deleteLater()
                self.infoLabel = QLabel(self)
            self.infoLabel.setLayout(window(person))
            # print(person.get_str())
        self.initImage()
        self.textLabel.setVisible(False)
        print("\n")
    def initMenu(self):
        menu = self.menuBar()
        fileMenu = menu.addMenu('File')
        load = fileMenu.addAction('Load')
        load.triggered.connect(self.openFileNameDialog)

    def initImage(self):
        image = QPixmap()
        image.loadFromData(self.name_image)
        self.pictureLabel.setGeometry(self.left, self.top, image.width(), image.height())
        self.pictureLabel.setPixmap(image)
        self.pictureLabel.show()
        # Set move Text Label
        self.infoLabel.setGeometry(image.width() + 50, self.top, 400, 1000)
        self.infoLabel.layout().update()
        self.infoLabel.show()

        self.infoLabelImage.setGeometry(self.infoLabel.width()+50,self.top,400,1000)
        self.infoLabelImage.show()

    def initTextLabel(self):
        self.textLabel.move(0, 20)
        self.textLabel.setText(self.textMainLabel)
        self.textLabel.setFont(FontBold())
        self.textLabel.show()
        self.textLabel.setMaximumWidth(300)
        self.textLabel.setMinimumWidth(300)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
